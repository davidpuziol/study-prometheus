# Grafana

[https://grafana.com/docs/grafana/latest/introduction/]

É possível também criar painéis usando a própria gui do prometheus usando a linguagemm GO. Porém o próprio Prometheus sugere o uso do [Grafana](https://grafana.com/) para esse serviço. Inclusive o grafana é está presente na CNCF.

Grafana permite consultar, visualizar, alertar e explorar suas métricas, logs e rastreamentos onde quer que estejam armazenados. O Grafana fornece ferramentas para transformar seus dados de banco de dados de série temporal (TSDB) em gráficos.

Depois de instalar o Grafana e configurar seu primeiro painel usando as instruções em Introdução ao Grafana , você terá muitas opções para escolher, dependendo de seus requisitos. Se você for o administrador de uma empresa e estiver gerenciando o Grafana para várias equipes, poderá configurar o provisionamento e a autenticação. O grafana também pode gerar alertas caso não seja utilizado o alertmanager o do prometheus. É uma questão de escolha.

A Grafana Labs possuem outros projetos além no grafana normal que vamos estudar. Deve ser entendido que grafana é um visualizador e os projetos abaixo entram como fonte de dados para ele, uma vez instalado. Além de gráficos o grafana pode prover outras visualizações.

- Grafana Loki: Grafana Loki é um conjunto de componentes de código aberto utilizados para registrar logs.
  - logs são eventos que acontecem no sistema ou na infra estrutura que são registrados de forma textuais. Servem principalmente para debugar a aplicação e conhecer a fonte do problema.
- Grafana Tempo: Grafana Tempo é um back-end de rastreamento distribuído de código aberto, fácil de usar e de alto volume. Para obter mais informações, consulte a documentação do Grafana Tempo .
- Grafana Mimir: Grafana Mimir é um projeto de software de código aberto que fornece um armazenamento escalável de longo prazo para o Prometheus. Para obter mais informações sobre o Grafana Mimir, consulte a documentação do Grafana Mimir .

## Instalação do grafana

Durante a instalação do prometheus para o kubernetes o helm utilizado já instala o grafana no kuberentes. Mas um studo exclusivo do grafana pode ser encontrado no repositório [https://gitlab.com/davidpuziol/study-grafana](https://gitlab.com/davidpuziol/study-grafana). Continue o estudo neste repositório.