# PromQL

[https://prometheus.io/docs/prometheus/latest/configuration/recording_rules/](https://prometheus.io/docs/prometheus/latest/configuration/recording_rules/)

Leitura extra no [book Promql](../books/prometheus-part-6-promql.pdf)

O Prometheus fornece uma linguagem de consulta funcional chamada PromQL (Prometheus Query Language) que permite ao usuário `selecionar e agregar` dados de séries temporais em tempo real. O resultado de uma expressão pode ser mostrado como um gráfico, visualizado como dados tabulares no navegador de expressão do Prometheus ou consumido por sistemas externos por meio da API HTTP.

Por que é necessário saber isso?
> Por que vc vai usar no grafana!

## Consulta

Retorna toda a time series da métrica

```bash
node_memory_MemFree_bytes
```

Retorne todas as séries temporais com a métrica http_requests_total e os job labels handler fornecidos:

```bash
node_memory_MemFree_bytes{instance="localhost:9100", job="david-pc"}
```

Retorne todo um intervalo de tempo (neste caso 5 minutos até o horário da consulta) para o mesmo vetor, tornando-o um vetor de intervalo :

```bash
node_memory_MemFree_bytes{instance="localhost:9100", job="david-pc"}[5m]
```

O tempo pode ser definido em:

- ms- milissegundos
- s- segundos
- m- minutos
- h- horas
- d- dias - supondo que um dia tenha sempre 24h
- w- semanas - assumindo que uma semana sempre tem 7d
- y- anos - assumindo que um ano sempre tem 365d

Observe que uma expressão que resulta em um vetor de intervalo não pode ser representada graficamente diretamente, mas visualizada na visualização tabular ("Console") do navegador de expressões.

Usando expressões regulares, você pode selecionar séries temporais apenas para trabalhos cujo nome corresponda a um determinado padrão, neste caso, todos os trabalhos que terminam com server:

```bash
node_cpu_scaling_frequency_hertz{job=~".*pc"}
```

- =: selecione rótulos que sejam exatamente iguais à string fornecida.
- !=: selecione rótulos que não sejam iguais à string fornecida.
- =~: selecione rótulos que correspondam à regex à string fornecida. Voce práticamente vai usar este.
- !~: selecione rótulos que não correspondam à regex da string fornecida.

>Todas as expressões regulares no Prometheus usam a sintaxe RE2. Para aprender regex recomendo o site [https://regexlearn.com/](https://regexlearn.com/)

Para selecionar todos os códigos de status HTTP, exceto os 4xx, você pode executar:

```bash
prometheus_http_requests_total{code!~"4.."}
```

## Operadores

Operadores binários aritméticos:

- +(Adição)
- -(subtração)
- *(multiplicação)
- /(divisão)
- %(módulo)
- ^(potência/exponenciação)

Operadores aritméticos binários são definidos entre pares de valores escalar/escalar, vetor/escalar e vetor/vetor.

Entre dois escalares , o comportamento é óbvio: eles avaliam para outro escalar que é o resultado do operador aplicado aos dois operandos escalares.

Entre um vetor instantâneo e um escalar , o operador é aplicado ao valor de cada amostra de dados no vetor. Por exemplo, se um vetor instantâneo de série temporal é multiplicado por 2, o resultado é outro vetor no qual cada valor de amostra do vetor original é multiplicado por 2. O nome da métrica é descartado.

Entre dois vetores instantâneos , um operador aritmético binário é aplicado a cada entrada no vetor do lado esquerdo e seu elemento correspondente no vetor do lado direito. O resultado é propagado no vetor de resultados com os rótulos de agrupamento tornando-se o conjunto de rótulos de saída. O nome da métrica é descartado. As entradas para as quais nenhuma entrada correspondente no vetor da direita pode ser encontrada não fazem parte do resultado.

Operadores binários de comparação

- `==`(igual)
- `!=`(não igual)
- `>`(Maior que)
- `<`(Menor que)
- `>=`(maior ou igual)
- `<=`(menor ou igual)

Os operadores de comparação são definidos entre pares de valores escalar/escalar, vetor/escalar e vetor/vetor. Por padrão, eles filtram. Seu comportamento pode ser modificado fornecendo boolapós o operador, que retornará 0ou 1 para o valor em vez de filtrar.

Entre dois escalares , o boolmodificador deve ser fornecido e esses operadores resultam em outro escalar que é 0( false) ou 1 ( true), dependendo do resultado da comparação.

Entre um vetor instantâneo e um escalar , esses operadores são aplicados ao valor de cada amostra de dados no vetor e elementos vetoriais entre os quais o resultado da comparação é falsedescartado do vetor resultante. Se o bool modificador for fornecido, os elementos do vetor que seriam descartados terão o valor 0e os elementos do vetor que seriam mantidos terão o valor 1. O nome da métrica será descartado se o boolmodificador for fornecido.

Entre dois vetores instantâneos , esses operadores se comportam como um filtro por padrão, aplicado às entradas correspondentes. Elementos de vetor para os quais a expressão não é verdadeira ou que não encontram uma correspondência no outro lado da expressão são descartados do resultado, enquanto os outros são propagados em um vetor de resultado com os rótulos de agrupamento tornando-se o conjunto de rótulos de saída. Se o boolmodificador for fornecido, os elementos do vetor que seriam descartados terão o valor 0e os elementos do vetor que seriam mantidos terão o valor 1, com os rótulos de agrupamento tornando-se novamente o conjunto de rótulos de saída. O nome da métrica será descartado se o boolmodificador for fornecido.

Operadores binários lógicos/definidos
Esses operadores binários lógicos/conjuntos são definidos apenas entre vetores instantâneos:

- `and`(interseção)
- `or`(União)
- `unless`(complemento)

vector1 and vector2resulta em um vetor que consiste nos elementos de vector1para os quais existem elementos vector2com conjuntos de rótulos exatamente correspondentes. Outros elementos são descartados. O nome e os valores da métrica são transferidos do vetor do lado esquerdo.

vector1 or vector2resulta em um vetor que contém todos os elementos originais (conjuntos de rótulos + valores) vector1e, adicionalmente, todos os elementos dos vector2 quais não possuem conjuntos de rótulos correspondentes em vector1.

vector1 unless vector2resulta em um vetor que consiste nos elementos de vector1para os quais não há elementos vector2com conjuntos de rótulos exatamente correspondentes. Todos os elementos correspondentes em ambos os vetores são descartados.

### Precedência do operador binário

A lista a seguir mostra a precedência de operadores binários no Prometheus, do maior para o menor.

- 1 ^
- 2 *, /, %,atan2
- 3 +,-
- 4 ==, !=, <=, <, >=,>
- 5 and,unless
- 6 or

>Operadores no mesmo nível de precedência são associativos à esquerda. Por exemplo, 2 * 3 % 2 é equivalente a (2 * 3) % 2. Contudo ^é associativo à direita, então 2 ^ 3 ^ 2 é equivalente a 2 ^ (3 ^ 2).

Correspondência de vetores
As operações entre vetores tentam encontrar um elemento correspondente no vetor do lado direito para cada entrada no lado esquerdo. Existem dois tipos básicos de comportamento de correspondência: um para um e muitos para um/um para muitos.

Palavras-chave correspondentes a vetores
Essas palavras-chave de correspondência de vetor permitem a correspondência entre séries com diferentes conjuntos de rótulos, fornecendo:

- on
- ignoring

As listas de rótulos fornecidas para palavras-chave correspondentes determinarão como os vetores são combinados. Exemplos podem ser encontrados em Correspondências de vetores um-para-um e em Correspondências de vetores muitos-para-um e um-para-muitos

Modificadores de grupo
Esses modificadores de grupo permitem a correspondência de vetores muitos-para-um/um-para-muitos:

- group_left
- group_right

As listas de rótulos podem ser fornecidas ao modificador de grupo que contém rótulos do lado "um" a serem incluídos nas métricas de resultado.

A correspondência muitos-para-um e um-para-muitos são casos de uso avançados que devem ser cuidadosamente considerados. Freqüentemente, o uso adequado de ignoring(<labels>)fornece o resultado desejado.

Os modificadores de agrupamento só podem ser usados ​​para comparação e aritmética . As operações como and, unlesse oras operações correspondem a todas as entradas possíveis no vetor correto por padrão.

Correspondências vetoriais um-para-um
Um-para-um localiza um par exclusivo de entradas de cada lado da operação. No caso padrão, essa é uma operação seguindo o formato vector1 <operator> vector2. Duas entradas correspondem se tiverem exatamente o mesmo conjunto de rótulos e valores correspondentes. A palavra- ignoringchave permite ignorar certos rótulos durante a correspondência, enquanto a palavra- onchave permite reduzir o conjunto de rótulos considerados a uma lista fornecida:

method_code:http_errors:rate5m{code="500"} / ignoring(code) method:http_requests:rate5m
Isso retorna um vetor de resultado contendo a fração de solicitações HTTP com código de status de 500 para cada método, medido nos últimos 5 minutos. Sem ignoring(code)isso, não haveria correspondência, pois as métricas não compartilham o mesmo conjunto de rótulos. As entradas com métodos pute delnão têm correspondência e não aparecerão no resultado:

{method="get"}  0.04            //  24 / 600
{method="post"} 0.05            //   6 / 120
Correspondências vetoriais muitos-para-um e um-para-muitos
Correspondências muitos -para-um e um-para-muitos referem-se ao caso em que cada elemento do vetor no lado "um" pode corresponder a vários elementos no lado "muitos". Isso deve ser explicitamente solicitado usando os modificadoresgroup_left or , onde left/right determina qual vetor tem a maior cardinalidade.group_right

<vector expr> <bin-op> ignoring(<label list>) group_left(<label list>) <vector expr>
<vector expr> <bin-op> ignoring(<label list>) group_right(<label list>) <vector expr>
<vector expr> <bin-op> on(<label list>) group_left(<label list>) <vector expr>
<vector expr> <bin-op> on(<label list>) group_right(<label list>) <vector expr>
A lista de rótulos fornecida com o modificador de grupo contém rótulos adicionais do lado "um" a serem incluídos nas métricas de resultado. Pois onum rótulo só pode aparecer em uma das listas. Cada série temporal do vetor de resultado deve ser identificável exclusivamente.

Consulta de exemplo:

method_code:http_errors:rate5m / ignoring(code) group_left method:http_requests:rate5m
Nesse caso, o vetor esquerdo contém mais de uma entrada por methodvalor de rótulo. Assim, indicamos isso usando group_left. Os elementos do lado direito agora correspondem a vários elementos com o mesmo methodrótulo à esquerda:

{method="get", code="500"}  0.04            //  24 / 600
{method="get", code="404"}  0.05            //  30 / 600
{method="post", code="500"} 0.05            //   6 / 120
{method="post", code="404"} 0.175           //  21 / 120
Operadores de agregação
O Prometheus suporta os seguintes operadores de agregação integrados que podem ser usados ​​para agregar os elementos de um único vetor instantâneo, resultando em um novo vetor de menos elementos com valores agregados:

sum(calcule a soma sobre as dimensões)
min(selecione o mínimo sobre as dimensões)
max(selecione o máximo sobre as dimensões)
avg(calcule a média sobre as dimensões)
group(todos os valores no vetor resultante são 1)
stddev(calcule o desvio padrão da população sobre as dimensões)
stdvar(calcule a variância padrão da população sobre as dimensões)
count(contar o número de elementos no vetor)
count_values(conta o número de elementos com o mesmo valor)
bottomk(menores k elementos por valor de amostra)
topk(maiores k elementos por valor de amostra)
quantile(calcule o quantil φ (0 ≤ φ ≤ 1) sobre as dimensões)
Esses operadores podem ser usados ​​para agregar todas as dimensões do rótulo ou preservar dimensões distintas incluindo uma cláusula withoutou . byEssas cláusulas podem ser usadas antes ou depois da expressão.

<aggr-op> [without|by (<label list>)] ([parameter,] <vector expression>)
ou

<aggr-op>([parameter,] <vector expression>) [without|by (<label list>)]
label listé uma lista de rótulos sem aspas que podem incluir uma vírgula à direita, ou seja, ambos (label1, label2)e (label1, label2,)são sintaxes válidas.

withoutremove os rótulos listados do vetor de resultado, enquanto todos os outros rótulos são preservados na saída. byfaz o oposto e descarta rótulos que não estão listados na bycláusula, mesmo que seus valores de rótulo sejam idênticos entre todos os elementos do vetor.

parametersó é necessário para count_values, quantilee .topkbottomk

count_valuesgera uma série temporal por valor de amostra exclusivo. Cada série tem um rótulo adicional. O nome desse rótulo é fornecido pelo parâmetro de agregação e o valor do rótulo é o valor de amostra exclusivo. O valor de cada série temporal é o número de vezes que o valor da amostra esteve presente.

topke bottomksão diferentes de outros agregadores porque um subconjunto das amostras de entrada, incluindo os rótulos originais, é retornado no vetor de resultado. bye withoutsão usados ​​apenas para agrupar o vetor de entrada.

quantilecalcula o quantil φ, o valor classificado no número φ*N entre os N valores métricos das dimensões agregadas. φ é fornecido como o parâmetro de agregação. Por exemplo, quantile(0.5, ...)calcula a mediana, quantile(0.95, ...)o percentil 95. Para φ = NaN, NaNé retornado. Para φ < 0, -Infé retornado. Para φ > 1, +Infé retornado.

Exemplo:

Se a métrica http_requests_totaltivesse séries temporais que se espalham por application, instancee grouprótulos, poderíamos calcular o número total de solicitações HTTP vistas por aplicativo e grupo em todas as instâncias por meio de:

sum without (instance) (http_requests_total)
O que equivale a:

 sum by (application, group) (http_requests_total)
Se estivermos interessados ​​apenas no total de solicitações HTTP que vimos em todos os aplicativos, poderíamos simplesmente escrever:

sum(http_requests_total)
Para contar o número de binários executando cada versão de compilação, poderíamos escrever:

count_values("version", build_version)
Para obter as 5 maiores contagens de solicitações HTTP em todas as instâncias, poderíamos escrever:

topk(5, http_requests_total)


Operadores para histogramas nativos
Histogramas nativos são um recurso experimental. A ingestão de histogramas nativos deve ser habilitada por meio de um sinalizador de recurso . Uma vez que os histogramas nativos tenham sido ingeridos, eles podem ser consultados (mesmo depois que o sinalizador de recurso foi desabilitado novamente). No entanto, o suporte do operador para histogramas nativos ainda é muito limitado.

Os operadores lógicos/binários de conjunto funcionam como esperado, mesmo se amostras de histograma estiverem envolvidas. Eles apenas verificam a existência de um elemento do vetor e não mudam seu comportamento dependendo do tipo de amostra de um elemento (float ou histograma).

O operador binário +entre dois histogramas nativos e o sumoperador de agregação para agregar histogramas nativos são totalmente suportados. Mesmo que os histogramas envolvidos tenham layouts de bucket diferentes, os buckets são convertidos automaticamente de forma apropriada para que a operação possa ser executada. (Com os esquemas de balde atualmente suportados, isso é sempre possível.) Se qualquer um dos operadores tiver que somar uma combinação de amostras de histograma e amostras flutuantes, o elemento de vetor correspondente será totalmente removido do vetor de saída.

Todos os outros operadores não se comportam de maneira significativa. Eles tratam a amostra do histograma como se fosse uma amostra flutuante de valor 0 ou (no caso de operações aritméticas entre um escalar e um vetor) deixam a amostra do histograma inalterada. Esse comportamento mudará para um significativo antes que os histogramas nativos sejam um recurso estável.

Esta documentação é de código aberto . Por favor, ajude a melhorá-lo registrando problemas ou pull requests.


## Subconsulta

Retorne a taxa de 5 minutos da métrica http_requests_total métrica para os últimos 30 minutos, com uma resolução de 1 minuto.

```
rate(http_requests_total[5m])[30m:1m]
```

Este é um exemplo de uma subconsulta aninhada. A subconsulta da deriv função usa a resolução padrão. Observe que usar subconsultas desnecessariamente não é sensato.

```
max_over_time(deriv(rate(distance_covered_total[5s])[30s:5s])[10m:])
```

Usando funções, operadores, etc.
Retorne a taxa por segundo para todas as séries temporais com o http_requests_total nome da métrica, conforme medido nos últimos 5 minutos:

```
rate(http_requests_total[5m])
```

Supondo que http_requests_totalvtodas as séries temporais tenham os rótulos job (fan out por nome do trabalho) e instance(fanout por instância do trabalho), podemos querer somar a taxa de todas as instâncias, para obter menos séries temporais de saída, mas ainda preservar a job dimensão :

```
sum by (job) (
  rate(http_requests_total[5m])
)
```

Se tivermos duas métricas diferentes com os mesmos rótulos dimensionais, podemos aplicar operadores binários a elas e os elementos de ambos os lados com o mesmo conjunto de rótulos serão correspondidos e propagados para a saída. Por exemplo, esta expressão retorna a memória não utilizada em MiB para cada instância (em um agendador de cluster fictício expondo essas métricas sobre as instâncias que executa):

```
(instance_memory_limit_bytes - instance_memory_usage_bytes) / 1024 / 1024
```

A mesma expressão, mas somada por aplicação, poderia ser escrita assim:

```
sum by (app, proc) (
  instance_memory_limit_bytes - instance_memory_usage_bytes
) / 1024 / 1024
```

Se o mesmo agendador de cluster fictício expusesse métricas de uso de CPU como as seguintes para cada instância:

```
instance_cpu_time_ns{app="lion", proc="web", rev="34d0f99", env="prod", job="cluster-manager"}
instance_cpu_time_ns{app="elephant", proc="worker", rev="34d0f99", env="prod", job="cluster-manager"}
instance_cpu_time_ns{app="turtle", proc="api", rev="4d3a513", env="prod", job="cluster-manager"}
instance_cpu_time_ns{app="fox", proc="widget", rev="4d3a513", env="prod", job="cluster-manager"}
...
```

poderíamos obter os 3 principais usuários de CPU agrupados por aplicativo ( app) e tipo de processo ( proc) assim:

```
topk(3, sum by (app, proc) (rate(instance_cpu_time_ns[5m])))
```

Supondo que essa métrica contenha uma série temporal por instância em execução, você pode contar o número de instâncias em execução por aplicativo da seguinte forma:

```
count by (app) (instance_cpu_time_ns)
```

## Funções

<https://prometheus.io/docs/prometheus/latest/querying/functions/>

Não adianta abordar todas as funções aqui, basta olhar o link acima. Mas para conhecimento acredito que as principais para o mundo DevOps podem ser abordadas abaixo. Não adianta decorar, mas conhecer algumas e de acordo com o que for precisando consulte a documentação. Veremos mais quando abordar o grafana.

Usamos funções para gerar um novo grupo de amostras. Abaixo segue a função e a entrada que ela precisa função(params).

- `abs(v instant-vector)`: retorna o vetor de entrada com todos os valores de amostra convertidos em seu valor absoluto.
- `absent(v instant-vector)`:retorna um vetor vazio se o vetor passado para ele tiver quaisquer elementos ou o valor 1 se o vetor passado para ele não tiver elementos. Isso é útil para alertar quando não existe nenhuma série temporal para um determinado nome de métrica e combinação de rótulo.
  - absent(nonexistent{job="myjob"})
- `absent_over_time(v range-vector)`: absent_over_time(v range-vector)retorna um vetor vazio se o vetor de intervalo passado para ele tiver quaisquer elementos ou um vetor de 1 elemento com o valor 1 se o vetor de intervalo passado para ele não tiver elementos.
- `ceil(v instant-vector)`: arredonda os valores de amostra de todos os elementos v até o inteiro mais próximo.
- `changes(v range-vector)`: Para cada série temporal de entrada, changes(v range-vector)retorna o número de vezes que seu valor foi alterado dentro do intervalo de tempo fornecido como um vetor instantâneo.
- `clamp(v instant-vector, min scalar, max scalar)`: clamp(v instant-vector, min scalar, max scalar) fixa os valores de amostra de todos os elementos v para ter um limite inferior de mine um limite superior de max.
- `clamp_max(v instant-vector, max scalar)`: fixa os valores de amostra de todos os elementos vpara ter um limite superior de max
- `clamp_min(v instant-vector, min scalar)`: fixa os valores de amostra de todos os elementos vpara ter um limite inferior de min.
- `day_of_month(v=vector(time()) instant-vector)`: day_of_month(v=vector(time()) instant-vector)retorna o dia do mês para cada um dos horários fornecidos em UTC. Os valores retornados são de 1 a 31.
- `day_of_week(v=vector(time()) instant-vector)`: day_of_week(v=vector(time()) instant-vector)retorna o dia da semana para cada um dos horários especificados em UTC. Os valores retornados são de 0 a 6, onde 0 significa domingo etc.
- `day_of_year(v=vector(time()) instant-vector)`: day_of_year(v=vector(time()) instant-vector)retorna o dia do ano para cada uma das horas especificadas em UTC. Os valores retornados são de 1 a 365 para anos não bissextos e de 1 a 366 para anos bissextos.
- `days_in_month(v=vector(time()) instant-vector)`: days_in_month(v=vector(time()) instant-vector)retorna o número de dias no mês para cada um dos horários fornecidos em UTC.
- `minute(v=vector(time()) instant-vector)`: retorna o minuto da hora para cada um dos horários fornecidos em UTC. Os valores retornados são de 0 a 59.
- `hour(v=vector(time()) instant-vector`: hour(v=vector(time()) instant-vector)retorna a hora do dia para cada um dos horários fornecidos em UTC. Os valores retornados são de 0 a 23.
- `month(v=vector(time()) instant-vector)`: retorna o mês do ano para cada um dos horários fornecidos em UTC. Os valores retornados são de 1 a 12, onde 1 significa janeiro etc.
- `year(v=vector(time()) instant-vector)`: retorna o ano para cada um dos horários fornecidos em UTC.
- `time()`: retorna o número de segundos desde 1º de janeiro de 1970 UTC. Observe que isso não retorna a hora atual, mas a hora em que a expressão deve ser avaliada.
- `timestamp(v instant-vector)`: retorna o carimbo de data/hora de cada uma das amostras do vetor fornecido como o número de segundos desde 1º de janeiro de 1970 UTC.
- `delta(v range-vector)`: calcula a diferença entre o primeiro e o último valor de cada elemento da série temporal em um vetor, retornando um vetor instantâneo com os deltas fornecidos e rótulos equivalentes. A expressão de exemplo a seguir retorna a diferença na temperatura da CPU entre agora e 2 horas atrás:
  - delta(cpu_temp_celsius{host="zeus"}[2h])
  - somente deve ser usado com métricas do tipo gauge
- `idelta(v range-vector)`: calcula a diferença entre as duas últimas amostras no vetor de intervalo v, retornando um vetor instantâneo com os deltas fornecidos e rótulos equivalentes.
- `floor(v instant-vector)`: arredonda os valores de amostra de todos os elementos para vbaixo até o inteiro mais próximo.
- `holt_winters(v range-vector, sf scalar, tf scalar)`: holt_winters(v range-vector, sf scalar, tf scalar)produz um valor suavizado para séries temporais com base no intervalo em v. Quanto menor o fator de suavização sf, mais importância é dada aos dados antigos. Quanto maior o fator de tendência tf, mais tendências nos dados são consideradas. Ambos `sf` e `tf` devem estar entre 0 e 1.
- `increase(v range-vector)`:calcula o aumento na série temporal no vetor de intervalo. As quebras na monotonicidade (como reinicializações do contador devido a reinicializações de destino) são ajustadas automaticamente. O aumento é extrapolado para cobrir todo o intervalo de tempo conforme especificado no seletor de vetor de intervalo, de modo que seja possível obter um resultado não inteiro mesmo se um contador aumentar apenas em incrementos de número inteiro.
- `label_join(v instant-vector, dst_label string, separator string, src_label_1 string, src_label_2 string, ...)`une todos os valores de todos os src_labels using separatore retorna a série temporal com o rótulo dst_labelcontendo o valor associado. Pode haver qualquer número src_labelsnesta função.
  - Este exemplo retornará um vetor com cada série temporal tendo um foo rótulo com o valor a,b,cadicionado a ele: label_join(up{job="api-server",src1="a",src2="b",src3="c"}, "foo", ",", "src1", "src2", "src3")
- `label_replace(v instant-vector, dst_label string, replacement string, src_label string, regex string)`: corresponde a expressão regular regex ao valor do rótulo src_label. Se corresponder, o valor do rótulo dst_label na série temporal retornada será a expansão de replacement, juntamente com os rótulos originais na entrada. A captura de grupos na expressão regular pode ser referenciada com $1, $2, etc. Se a expressão regular não corresponder, a série temporal será retornada inalterada.
- `rate(v range-vector)`: calcula a taxa média por segundo de aumento da série temporal no vetor de alcance. As quebras na monotonicidade (como reinicializações do contador devido a reinicializações de destino) são ajustadas automaticamente. Além disso, o cálculo extrapola para o final do intervalo de tempo, permitindo raspagens perdidas ou alinhamento imperfeito dos ciclos de raspagem com o período de tempo do intervalo.
- `irate(v range-vector)`: calcula a taxa instantânea por segundo de aumento da série temporal no vetor de alcance. Isso é baseado nos dois últimos pontos de dados. As quebras na monotonicidade (como reinicializações do contador devido a reinicializações de destino) são ajustadas automaticamente.
- `resets(v range-vector)`: retorna o número de redefinições do contador dentro do intervalo de tempo fornecido como um vetor instantâneo. Qualquer diminuição no valor entre duas amostras consecutivas é interpretada como uma reinicialização do contador.
  - resets só deve ser usado com counters.
- `round(v instant-vector, to_nearest=1 scalar)`: arredonda os valores de amostra de todos os elementos v para o inteiro mais próximo. Os empates são resolvidos por arredondamento. O argumento opcional to_nearest permite especificar o múltiplo mais próximo para o qual os valores de amostra devem ser arredondados. Este múltiplo também pode ser uma fração.
- `sgn(v instant-vector)`: retorna um vetor com todos os valores de amostra convertidos em seus sinais, definidos como: 1 se v for positivo, -1 se v for negativo e 0 se v for igual a zero.
- `sort(v instant-vector)`: retorna elementos de vetor classificados por seus valores de amostra, em ordem crescente.
- `sort_desc(v instant-vector)`: O mesmo que sort, mas classificado em ordem decrescente.
- `vector(s scalar)`: retorna o escalar s como um vetor sem rótulos.

As funções a seguir permitem agregar cada série de um determinado vetor de intervalo ao longo do tempo e retornar um vetor instantâneo com resultados de agregação por série:

- `avg_over_time(range-vector)`: o valor médio de todos os pontos no intervalo especificado.
- `min_over_time(range-vector)`: o valor mínimo de todos os pontos no intervalo especificado.
- `max_over_time(range-vector)`: o valor máximo de todos os pontos no intervalo especificado.
- `sum_over_time(range-vector)`: a soma de todos os valores no intervalo especificado.
- `count_over_time(range-vector)`: a contagem de todos os valores no intervalo especificado.
- `quantile_over_time(scalar, range-vector)`: o quantil φ (0 ≤ φ ≤ 1) dos valores no intervalo especificado.
- `stddev_over_time(range-vector): o desvio padrão populacional dos valores no intervalo especificado.
- `stdvar_over_time(range-vector): a variância padrão da população dos valores no intervalo especificado.
- `last_over_time(range-vector)`: o valor de ponto mais recente no intervalo especificado.
- `present_over_time(range-vector)`: o valor 1 para qualquer série no intervalo especificado.
Observe que todos os valores no intervalo especificado têm o mesmo peso na agregação, mesmo que os valores não estejam igualmente espaçados ao longo do intervalo.

>Ainda existem funções logaríticas, trigonométricas, dentre outras.
