# Architetura

O Prometheus trabalha ativamente, ele é quem vai até um endpoint exposto pela aplicação para buscar as métricas através de pull. Porém estas métricas tem que estar formatada de acordo para entrar no TSDB (Time Series Data Base).

Geralmente em outras ferramentas, a aplicação que envia os dados através de um push.

![Architetura](../pics/arch.png)

## Prometheus Server

Dentro do prometheus server temos os seguintes elementos:

- TSDB
- Retrieval
- Http (PromQL)

### TSDB (Time Series Data Base)

<https://prometheus.io/docs/prometheus/latest/storage/>

É utilizado para armazenar os dados de métricas.

O Prometheus inclui um banco de dados de série temporal local em disco, mas também se integra opcionalmente com sistemas de armazenamento remoto.

O prometheus por defaul (pode ser configurado)separa os arquivos em blocos de duas em duas horas consegue guardar seus valores coletados em diferentes arquivos. Cada bloco de duas horas consiste em um diretório contendo um subdiretório de chunks contendo todas as amostras de séries temporais para aquela janela de tempo, um arquivo de metadados e um arquivo de índice (que indexa nomes de métricas e rótulos para séries temporais no diretório de chunks). As amostras no diretório chunks são agrupadas em um ou mais arquivos de segmento de até 512 MB cada por padrão. Quando as séries são excluídas por meio da API, os registros de exclusão são armazenados em arquivos de exclusão separados (em vez de excluir os dados imediatamente dos segmentos do bloco).

> Separar melhora a perfformance e diminui o consumo de memória, pois não terá um arquivo tão grande ocupando espaço.

O bloco atual para as amostras recebidas é mantido na memória e não é totalmente persistente. Ele é protegido contra travamentos por um log write-ahead (WAL) que pode ser reproduzido quando o servidor Prometheus for reiniciado.

> Observe que uma limitação do armazenamento local é que ele não é agrupado ou replicado. O uso de RAID é sugerido para dispobilidade de armazenamento e snapshots são recomendados para backup.

Também é possível utilizar outros banco de dados ao invés de salvar em arquivo. Para isso é necessário o uso de adapter de acordo com o banco onde você irá guardar os dados. Porém existe uma perde de desempenho e eficiência.

Com o passar do tempo os bloco de duas horas são compactados em bloco mais longos, geralmente 31 dias. Essa compactação é o que causa uma perda de precisão nos dados coletados.

> Por isso o prometheus não é recomendado para métricas de negócio.

### Retrieval

Responsável por ir buscar as métricas no jobs (aplicações). Abaixo um exemplo de como as métricas devem ser formatadas.

![Metrics](../pics/examplemetrics.png)

Várias bibliotecas são suporte a isso. Confira no link <https://prometheus.io/docs/instrumenting/clientlibs/>

Além disso existem bibliocas prontas como o OpenTelemetry que podem ser usadas.

#### Integração Nativa

Algumas ferramentas de mercado já possuem suporte nativo para o prometheus expondo endpoints de métricas para o prometheus somente com uma configuração simples.

- Grafana
- Docker
- Kubernetes
- Traefik
- Gitlab
- Netdata
- RabbitMQ
- Etcd
- ...

#### Exporters

Quando uma ferramenta não tem suporte ao prometheus (não expoe suas métricas, ou não utilizam os padrões necessários para o prometheus) usamos o que chamamos de exporters para esse cenário.

Os exporters são processos que vão coletar as métricas dessas aplicações expor elas para o prometheus coletar essas métricas.

[Lista de exporters](https://prometheus.io/docs/instrumenting/exporters/)

#### Push Gateway

Quando um processo possui uma curta duração (Short-lived jobs) e não da tempo do prometheus buscar as métricas, utilizamos um recurso chamado `Push Gateway`. Nesse caso cabe a esse processo enviar suas métricas para o push gateway e o prometheus buscará nele. Serve como se fosse um cache.

#### Service Discovery

Para o retrivel conseguir encontrar os endpoints de aplicações que ficam mudando ip e escalando dinamicamente, o prometheus dispõe da funcionalidade de service discovery e dá suporte a vários serviços como kubernetes, consul, etc. Isso é muito bom para o cenário de microserviços.

<https://prometheus.io/docs/prometheus/latest/configuration/configuration/>

### HTTP server

Esta é a parte que faz a iteração do prometheus com o usuário.

- WEB UI
- API
- Grafana ou algum outro dashboard

### Alert Manager

Não é responsabilidade do prometheus entregar o alerta para o usuário quando este acontecer. Para isso ele utiliza um outro sistema chamado Alert Manager.

Observe que o Alert Manager é um app a parte do grafana no qual o grafana envia para ele um alerta e cabe a ele enviá-lo para o usuário seja através do slack, telegram, email ou algum outro push notification.

## O Prometheus pode ser altamente disponível?

Sim, execute servidores Prometheus idênticos em duas ou mais máquinas separadas. Alertas idênticos serão desduplicados pelo Alertmanager .

Para alta disponibilidade do Alertmanager , você pode executar várias instâncias em um cluster Mesh e configurar os servidores Prometheus para enviar notificações para cada um deles.
