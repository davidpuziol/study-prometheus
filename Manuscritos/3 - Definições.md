# Definições

## Tipos de métricas

Para o prometheus ainda não existe diferença entre os tipos de métricas. Ele converte todas as métricas para série temporal.

- Counter
  - Um contador é uma métrica cumulativa que representa um único contador de aumento monotônico cujo valor só pode aumentar ou ser redefinido para zero na reinicialização
- Gauge
  - Um medidor é uma métrica que representa um único valor numérico que pode subir e descer arbitrariamente.
- Histogram
  - Um histograma mostra observações (geralmente coisas como durações de solicitação ou tamanhos de resposta) e as conta em baldes configuráveis. Ele também fornece uma soma de todos os valores observados.
- Summary
  - Semelhante a um histograma , um resumo mostra observações (geralmente coisas como durações de solicitação e tamanhos de resposta). Embora também forneça uma contagem total de observações e uma soma de todos os valores observados, ele calcula quantis configuráveis ​​em uma janela de tempo variável.

## Modelo de dados

Cada série temporal é identificada exclusivamente por seu nome de métrica e pares de valores-chave opcionais chamados labels.
O nome da métrica especifica o recurso geral de um sistema que é medido (por exemplo http_requests_total, o número total de solicitações HTTP recebidas).
Pode conter letras e dígitos ASCII, bem como sublinhados e dois-pontos. Deve corresponder ao regex `[a-zA-Z_:][a-zA-Z0-9_:]*`.

1 - Somente pode ser usado letras maiusculas e minusculas, numeros e undeline(_)
2 - Não se deve iniciar uma metrica com underline, pois elas representam métricas de uso interno do prometheus.
3 - O dois pontos (:) são reservados para regras de gravação definidas pelo usuário. Eles não devem ser usados ​​por exportadores ou instrumentação direta.
4 - Os valores de labels podem conter qualquer caractere Unicode.

Os labels ativam o modelo de dados dimensionais do Prometheus: qualquer combinação de labels para o mesmo nome de métrica identifica uma instanciação dimensional específica dessa métrica (por exemplo: todas as solicitações HTTP que usaram o método POST para o /api/tracksmanipulador). A linguagem de consulta permite filtragem e agregação com base nessas dimensões. Alterar qualquer valor de labels, incluindo adicionar ou remover uma label, criará uma nova série temporal.

>Uma label com um valor vazio é considerado não existente.

As amostras formam os dados reais da série temporal. Cada amostra consiste em:

- um valor float64
- um carimbo de data/hora com precisão de milissegundos

Dado um nome de métrica e um conjunto de labels, as séries temporais são frequentemente identificadas usando esta notação:

`<metric name>{<label name>=<label value>, ...}`

Por exemplo, uma série temporal com o nome da métrica api_http_requests_totale os rótulos method="POST"e handler="/messages"poderia ser escrita assim:

`api_http_requests_total{method="POST", handler="/messages"}`

Esta é a mesma notação que o OpenTSDB usa.

## Jobs vs Instances

Nos termos do Prometheus, um endpoint que você pode fazer o scrape é chamado de instância , geralmente correspondendo a um único processo. Uma coleção de instâncias com o mesmo propósito, um processo replicado para escalabilidade ou confiabilidade, por exemplo, é chamada de job .

Por exemplo, um job de servidor de API com quatro instâncias replicadas:

- job: api-server
  - instance 1: 1.2.3.4:5670
  - instance 2: 1.2.3.4:5671
  - instance 3: 5.6.7.8:5670
  - instance 4: 5.6.7.8:5671

Quando o Prometheus faz o scrape em um target, ele anexa alguns rótulos automaticamente à série temporal scraped que serve para identificar o target:

- `job`: O nome do job configurado que o target pertence.
- `ìnstance`:  O `<host>:<port>` da url que foi feita o scrape
