# Instalação

Inicialmente a instalação do prometheus é somente do prometheus e não a stack completa com o grafana e o alert manager que são serviços a parte e isso deve ficar bem claro.

O grafana pode ser configurado com uma entrada de dados vindo do prometheus assim como o alertmanager também é configurado para que o prometheus envie o alerta.

## Instalação local

Na página de release podemos baixar projeto do prometheus. [https://github.com/prometheus/prometheus/releases/](https://github.com/prometheus/prometheus/releases/)

```bash
# Criando usuário e grupo de serviço do prometheus
sudo groupadd --system prometheus
sudo useradd -s /sbin/nologin --system -g prometheus 

# Criando arvore de diretório no /etc/prometheus
# /etc/prometheus/files_sd
# /etc/prometheus/rules
# /etc/prometheus/rules.d
for i in rules rules.d files_sd; do sudo mkdir -p /etc/prometheus/${i}; done

# /Fazendo o download do prometheus no /tmp/prometheus
mkdir -p /tmp/prometheus && cd /tmp/prometheus
curl -s https://api.github.com/repos/prometheus/prometheus/releases/latest | grep browser_download_url | grep linux-amd64 | cut -d '"' -f 4 | wget -qi -

# unzip
tar xvf prometheus*.tar.gz
# Movendo o binário para o path do sistema
cd prometheus*/
sudo mv prometheus promtool /usr/local/bin/
# Movendo arquivo de configuração e pastas para /etc/prometheus
sudo mv prometheus.yml /etc/prometheus/prometheus.yml
sudo mv consoles/ console_libraries/ /etc/prometheus/

# Corrigindo as permissões 
sudo mkdir /var/lib/prometheus
sudo chown -R prometheus:prometheus /etc/prometheus/
sudo chmod -R 775 /etc/prometheus/
sudo chown -R prometheus:prometheus /var/lib/prometheus/
sudo chown -R prometheus:prometheus /var/log/prometheus/

# Deletando o lixo
rm -rf /tmp/prometheus

prometheus --version
promtool --version

# Caso necessário altere o arquivo de configuração do /etc/prometheus/prometheus.yml

# Criando o serviço
sudo tee /etc/systemd/system/prometheus.service<<EOF
[Unit]
Description=Prometheus
Documentation=https://prometheus.io/docs/introduction/overview/
Wants=network-online.target
After=network-online.target
ConditionFileNotEmpty=/etc/prometheus/prometheus.yml

[Service]
Type=simple
User=prometheus
Group=prometheus
ExecReload=/bin/kill -HUP \$MAINPID
ExecStart=/usr/local/bin/prometheus \
  --config.file=/etc/prometheus/prometheus.yml \
  --storage.tsdb.path=/var/lib/prometheus \
  --web.console.templates=/etc/prometheus/consoles \
  --web.console.libraries=/etc/prometheus/console_libraries \
  --web.listen-address=0.0.0.0:9090 \
  --web.external-url=
SyslogIdentifier=prometheus
Restart=always
pid_file="/var/lib/prometheus/prometheus.pid"
StandardOutput=append:/var/log/prometheus/output.log
StandardError=append:/var/log/prometheus/error.log

[Install]
WantedBy=multi-user.target
EOF

# Liberando a porta 9090 no firewall
sudo ufw allow 9090/tcp 

# Ativando o serviço
sudo systemctl daemon-reload
sudo systemctl enable prometheus --now
sudo systemctl status prometheus
```

Em tese o prometheus já está instalado respondendo em localhost:9090, mas não esta colhendo nenhuma métrica.
Porém se vc quer instalar o prometheus na sua máquina linux, ela não tem integração natural para o prometheus ir buscar as métricas. Por isso precisamos de um node exporter. Nesse caso é o serviço que irá ler o /proc do linux e entregar tudo para o prometheus formatado.

```bash
sudo useradd -M -r -s /bin/false node_exporter
mkdir -p /tmp/node_exporter && cd /tmp/node_exporter
curl -s https://api.github.com/repos/prometheus/node_exporter/releases/latest| grep browser_download_url| grep linux-amd64| cut -d '"' -f 4| wget -qi -
tar -xvf node_exporter*.tar.gz
cd  node_exporter*/
sudo cp node_exporter /usr/local/bin
node_exporter --version

cat << EOF | sudo tee /etc/systemd/system/node_exporter.service
[Unit]
Description=Node Exporter
Wants=network-online.target
After=network-online.target

[Service]
User=node_exporter
ExecStart=/usr/local/bin/node_exporter

[Install]
WantedBy=default.target
EOF

sudo ufw allow 9100

sudo systemctl daemon-reload
sudo systemctl enable node_exporter --now
sudo systemctl status node_exporter.service 
```

Temos o node exporter rodando, porém o prometheus ainda não está sabe que precisa fazer o scrape para o node exporter, necessitamos mexer na configuração do prometheus em /etc/prometheus/prometheus.yml.

Precisamos adicionar um job no prometheus na tag scrape_config como abaixo:

```yaml
...
scrape_configs:
  # The job name is added as a label `job=<job_name>` to any timeseries scraped from this config.
  - job_name: 'prometheus'

    # metrics_path defaults to '/metrics'
    # scheme defaults to 'http'.

    static_configs:
    - targets: ['localhost:9090']

####### Essa parte #######
  - job_name: david-pc
    static_configs:
      - targets: ['localhost:9100']
```

Agora reinicie o prometheus para que ele pegue as configurações.

```bash
sudo systemctl restart prometheus
```

![prometheus targets](../pics/targets.png)
![prometheus search](../pics/prometheusinstallNE.png)

## Instalação docker

Para instalar via docker, uma vez que já temos um arquivo de configuração feito e um node exporter instalado localmente, podemos somente subir o serviço em outra porta para teste. Porém dessa vez precisamos apontar o ip, pois localhost não irá funcionar.

```bash
# -v path_origem:/etc/prometheus/prometheus.yml
# -p porta
# Lembre de mudar o arquivo resources/prometheus.yml para o ip da sua máquina
docker run \
    -p 9091:9090 \
    -v /home/dprata/Desktop/personal/study-prometheus/resources/prometheus.yml:/etc/prometheus/prometheus.yml \
    prom/prometheus
```

![docker](../pics/docker.png)

## Instalação no Kubernetes

[https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack](https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack)

Para o kubernetes vamos fazer a instalação do prometheus utilizando o helm com um chart que instalada uma stack completa do prometheus com alertmanager e o grafana.

>É necessário ter um cluster kubernetes e o helm instalado.

Vamos primeiro baixar o values para configuração

```bash
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
```

Vamos somente instalar o padrão sem alterar nada, pois mais pra frente vamos configurar melhor isso dai.

```bash
helm install prometheus-stack prometheus-community/kube-prometheus-stack --create-namespace --namespace monitoring

kubectl get pods -n monitoring
NAME                                                     READY   STATUS    RESTARTS   AGE
prometheus-stack-prometheus-node-exporter-mbfkh          1/1     Running   0          34s
prometheus-stack-prometheus-node-exporter-28ct7          1/1     Running   0          34s
prometheus-stack-prometheus-node-exporter-5s75j          1/1     Running   0          34s
prometheus-stack-grafana-bd7d589f-ch6sg                  3/3     Running   0          34s
prometheus-stack-kube-prom-operator-947444c9b-vpg4d      1/1     Running   0          34s
prometheus-stack-kube-state-metrics-676cffd69c-tjxgp     1/1     Running   0          34s
alertmanager-prometheus-stack-kube-prom-alertmanager-0   2/2     Running   0          29s
prometheus-prometheus-stack-kube-prom-prometheus-0       2/2     Running   0          29s
```

Observe que temos 3 nodes exporters, um para cada node do cluster que tenho.

## Extras

Leitura complementar dos books?

- [book install](../books/prometheus-part-4-install-and-configurations.pdf).
- [book expressoes](../books/prometheus-part-5-visulization-using-expression-browser.pdf)
  - Faça alguns testes na sua instalação.
