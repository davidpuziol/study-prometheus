# Study Prometheus

![prometheus](./pics/prometheus.svg)

O Prometheus é um kit de ferramentas de monitoramento e alerta de sistemas de código aberto com um ecossistema ativo. É o único sistema suportado diretamente pelo Kubernetes e o padrão de fato em todo o ecossistema nativo da nuvem. É um projeto open source graduado pela CNCF e escrito em golang.

- Armazena e coleta dados de metricas em series temporais, ou seja, carimbada com data e hora em que foram registradas.
- Pode usar tags inclusive para armazenamento das métricas

[Github Prometheus](https://github.com/prometheus/prometheus)

## Funcionalidades

- modelo de dados multi dimensional dados de métrica em series identificadas por nome para chave/valor
- Utiliza a linguaguem PromQL que é mais poderosa que a SQL para monitoramento.
- Nodes independentes e autônomos sem utilização de um sistema distribuído.
- Métricas são coletadas via HTTP. O prometheus vai até a aplicação e coleta no endpoint especificado.
- Entrega de métricas pode utilizar um gateway intermediário
- O destino pode ser descoberto via service discovery ou configuração estatica.
- Suporta vários dashboard do mercado
- Configuração de alertas

## Métricas

São medições numérica de dados relacionados a elementos do seu softwares ou da infra. Normalmente esses dados numéricos são relacionados por uma linha temporal.

Série temporal signiffica que mudanças são registradas ao longo do tempo.

 Métricas do Sistema | Métricas de Negócio |
|---|---|
| Latência | Número de usuários acessando aplicação |
| Quantidade de Requisições | Número de boletos emitidos |
| Consumo de recursos | Compras de um determinado produto |
| APIs mais acessadas | Faturamento diario |
| Quantidade de erros |  |

Por exemplo, o aplicativo pode ficar lento quando o número de solicitações é alto. Se você tiver a métrica de contagem de solicitações, poderá identificar o motivo e aumentar o número de servidores para lidar com a carga.

> `Métrica não é log`

 Métricas | Log |
|---|---|
| Dados numéricos | Dados textuais |
| Gráfico | Mensagens de erro |
| Agregações | Informações |
| Performance | Buscáveis |

## Prometheus vs InfluxDB

Essas são as duas principais ferramentas de monitoramento hoje. Qual a diferença e quando usar cada uma das duas?

Em tese os dois fazem praticamente de forma idêntica:

- Compactação de dados
- Dados multidimensionais (Prometheus usa labels e Influx tags)
- Sistema de alerta (Alertmanager no Prometheus e Kapacitor no InfluxDB)
- Linguagem de consulta para interagir com as métricas e analisá-las
- Ambos tem integração com muitas outras ferramentas. O Prometheus um pouco mais, mas a grande maioria do que precisamos tem nas duas. O que não tem de integração pode ser resolvido com webhook em ambos.
- Comunidade de desenvolvedores grande. O Prometheus um pouco mais.

Diferença entre eles:

- InfluxDB tende a ser usado para um banco de dados de séries temporais e o prometheus tende ser mais usado no propósito de monitoramento.
- InfluxDB tem uma linguagem própria FluxQL e o Prometheus o PromQL.
  - O PromQL é mais facil e desenvolvido para fins de monitoramento, alerta e gráficos. O software de banco de dados Prometheus assume automaticamente muitas coisas sobre nossa consulta e não precisamos fornecer todas as etapas.
  - O FluxQL é necessário a passagem de mais parâmetros pois o InfluxDB é um banco de dados de série temporal para uso geral, enquanto o Prometheus foi desenvolvido especialmente para monitoramento.
- InfluxDB suporta tipos float64, int64, bool e string e o Prometheus float64 e strings
- InfluxDB grava dados com registro de data e hora até em nanosegundos e o Prometheus em milisegundos.
- InfluxDB não extrai métricas períocamente do sistema destino, ele espera que um aplicativo envie os dados para ele. O Prometheus pode extrair as métricas do sistema alvo.
- InfluxDB possue problema com alto consumo de memória e CPU em relação ao Prometheus.
- InfluxDB tem como objetivo guardar todo tipo de dados por muito tempo enquanto o Prometheus destina-se a guardar dados por padrão apenas de 15 dias, mas pode ser alterado.
- Configuração no InfluxDB é feita por chamadas de API enquanto no Prometheus é por arquivo yaml. Nesse caso a configuração com o prometheus é mais façil inclusive. No InfluxDB é dificil garantir que as alterações sejam idempotentes por que vc pode invocar o script de configuração varias vezes.

O InfluxDB é uma ferramentas mais poderosa e para grande volume de dados e de uso geral. Por outro lado o prometheus tem uma instalação mais simples, uma comunidade maior.

Qualquer uma das ferramentas vai te atender bem. Mas fica em mente que se vc só quer monitorar algo, pra que usar uma basuca pra matar um mosquito?

## Quando não usar o Prometheus?

>A compactação de dados antigos pelo prometheus faz com que a precisão não seja 100%. Ele calcula umas médias e substitui os dados para diminuir o uso de disco. Isso pode ser configurado obviamente, mas o consumo de disco irá aumentar muito.

Se você precisa de 100% de precisão, como para cobrança por solicitação, o Prometheus não é uma boa escolha, pois os dados coletados provavelmente não serão detalhados e completos o suficiente. Nesse caso, seria melhor usar algum outro sistema para coletar e analisar os dados para cobrança e o Prometheus para o restante do monitoramento.

## Como estudar?

Deve seguir o passo a passo e fazer a leituras complementares dos books.

- Leitura complementar iniciais
  - [Por que monitorar?](./books/prometheus-part-1-understanding-a-world-of-monitoring.pdf)
  - [O que é um time series database?](./books/prometheus-part-2-understanding-a-time-series-database.pdf)
- [1 - Arquitetura](./Manuscritos/1%20-%20Architetura.md)
- [2 - Instalação](./Manuscritos/1%20-%20Architetura.md)
- [3 - Definições](./Manuscritos/3%20-%20Defini%C3%A7%C3%B5es.md)
- [4 - Configurações](./Manuscritos/4%20-%20Configura%C3%A7%C3%B5es.md)
- [5 - PromQL](./Manuscritos/1%20-%20Architetura.md)